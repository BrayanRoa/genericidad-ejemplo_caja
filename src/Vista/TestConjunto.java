/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Util.Caja;
import Util.Conjunto;
import javax.swing.JOptionPane;

/**
 *
 * @author Lenovo
 */
public class TestConjunto {
    
    public static void main(String[] args) throws Exception {
        Conjunto<Estudiante> c2 = new Conjunto(3);
        Conjunto<Estudiante> c3 = new Conjunto(3);

        c2.adicionarElemento(new Estudiante(1522, "Santiago"));
        c2.adicionarElemento(new Estudiante(1524, "Felipe"));
        c2.adicionarElemento(new Estudiante(1529, "Maria"));
        //c2.adicionarElemento(new Estudiante(1527, "Mateo"));
        //c2.adicionarElemento(new Estudiante(1528, "Manuel"));*/
       
        
        c3.adicionarElemento(new Estudiante(1521, "Santiago"));
        c3.adicionarElemento(new Estudiante(1524, "Felipe"));
        c3.adicionarElemento(new Estudiante(1529, "Maria"));
        //c3.adicionarElemento(new Estudiante(1520, "Ricardo"));*/
        
       // c2.getInterseccion(c3);
        System.out.println("Intersectado:\n " + c2.getInterseccion(c3));
    }
}
        
    